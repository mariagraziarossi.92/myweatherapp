//
//  CustomCell.m
//  WeatherApp
//
//  Created by Mariagrazia Rossi on 21/02/2020.
//  Copyright © 2020 mariagrazia. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

- (void) configure:(Preview * _Nonnull)prev {
    _townLabel.text = prev.town;
    _temperatureLabel.text = prev.temperature;
    _hourLabel.text = prev.hour;
    if (prev.descr != nil){
        [_weatherImage setImage:[UIImage imageNamed:[prev.icon stringByReplacingOccurrencesOfString:@"n" withString:@"d"]]];
    }
   
}

- (void) configure:(NSString * _Nonnull)town :(NSString * _Nonnull)temperature :(NSString * _Nonnull)time {
    _townLabel.text = town;
    _temperatureLabel.text = temperature;
    _hourLabel.text = time;
}
@end
