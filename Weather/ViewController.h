//
//  ViewController.h
//  WeatherApp
//
//  Created by Mariagrazia Rossi on 20/02/2020.
//  Copyright © 2020 mariagrazia. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Preview;

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

