//
//  DetailViewController.swift
//  Weather
//
//  Created by Mariagrazia Rossi on 22/02/2020.
//  Copyright © 2020 mariagrazia. All rights reserved.
//

import UIKit

class ForecastCell : UITableViewCell {
    
    @IBOutlet weak var minTemperatureLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var maxTemperatureLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    func configure(withWeather w : Weather?){
        guard let w = w else {return}
        minTemperatureLabel.text = w.minTemp
        maxTemperatureLabel.text = w.maxTemp
        dateLabel.text = w.dt
        if let image = UIImage(named: (w.icon?.replacingOccurrences(of: "n", with: "d"))!){
            iconView.image = image
        }
        
    }
    
}

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!

    private var forecast = [Weather?]()
    private var actInd : UIActivityIndicatorView!
    @objc var cityPreview = Preview()
    
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        self.startActivityIndicator()
//        self.getForecast()
//    }
//    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureTopView()
        createActivityIndicator()
        self.startActivityIndicator()
        self.getForecast()
    }
    
    private func configureTopView(){
        cityLabel.text = String(cityPreview.town)
        temperatureLabel.text = String(cityPreview.temperature)
        descriptionLabel.text = String(cityPreview.descr ?? "--")
    }
    
    private func setupTableView(hidden: Bool){
    DispatchQueue.main.async {
            //Reload Table before hide it, to make sure that the logic keep working
            self.tableView.reloadData()
            self.tableView.isHidden = hidden
        }
    }
    
    private func getForecast(){
        self.setupTableView(hidden: true)
        WeatherManager.shared.fetchforecast(town: cityPreview.town, completion:
            { fArray in
                self.forecast = fArray
                self.setupTableView(hidden: false)
                self.stopActivityIndicator()
        })
    }
    
    //MARK: tableView
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastCell") as! ForecastCell
        if indexPath.row < forecast.count {
             cell.configure(withWeather: forecast[indexPath.row])
        } else  {
            cell.configure(withWeather:Weather())
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecast.count
    }
    
    //MARK: activityIndicator func
    func createActivityIndicator() {
        actInd = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        
        actInd.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        let transform: CGAffineTransform = CGAffineTransform(scaleX: 2, y: 2)
        actInd.transform = transform
        actInd.center = self.view.center
        actInd.center.y -= 100
        actInd.isHidden = true
        actInd.hidesWhenStopped = true
        self.view.addSubview(actInd)
    }
    
    func moveActIndCenterY(deltaY: CGFloat) {
        actInd.center.y -= deltaY
    }
    
    func startActivityIndicator() {
        DispatchQueue.main.async {
            self.actInd.isHidden = false
            self.view.isUserInteractionEnabled = false
            self.actInd.startAnimating()
        }
    }
    
    func stopActivityIndicator() {
        
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled = true
            self.actInd.stopAnimating()
            self.tableView.reloadData()
        }
    }
}
