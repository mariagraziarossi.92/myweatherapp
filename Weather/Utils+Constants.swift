//
//  Utils.swift
//  Weather
//
//  Created by Mariagrazia Rossi on 23/02/2020.
//  Copyright © 2020 mariagrazia. All rights reserved.
//

import Foundation

struct Constants {
    static let forecastUrl = "https://api.openweathermap.org/data/2.5/forecast?q=%@&APPID=4bc1f4ca7650ed56f0e2782424f660e3"
    static let currentWeatherUrl = "https://api.openweathermap.org/data/2.5/weather?q=%@&APPID=4bc1f4ca7650ed56f0e2782424f660e3"
}
