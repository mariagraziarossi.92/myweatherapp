//
//  CustomCell.h
//  WeatherApp
//
//  Created by Mariagrazia Rossi on 21/02/2020.
//  Copyright © 2020 mariagrazia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Weather-Swift.h"

NS_ASSUME_NONNULL_BEGIN
@class Preview;


@interface CustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *townLabel;
@property (weak, nonatomic) IBOutlet UILabel *hourLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UIImageView *weatherImage;

-(void)configure :(Preview * _Nonnull)prev;
-(void)configure :(NSString * _Nonnull)town :(NSString * _Nonnull)temperature :(NSString * _Nonnull)time;
@end

NS_ASSUME_NONNULL_END
