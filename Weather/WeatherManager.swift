//
//  WeatherManager.swift
//  Weather
//
//  Created by Mariagrazia Rossi on 22/02/2020.
//  Copyright © 2020 mariagrazia. All rights reserved.
//

import Foundation
import UIKit

@objc public class WeatherManager : NSObject {
    static let shared = WeatherManager()
    @objc var previewArray = [Preview]()
    
    enum WeatherDescription{
        case clear_sky, few_clouds, scatter_clouds, broken_clouds, shower_rain, rain, thunderstorm, snow, mist
        
        var image : UIImage{
            switch self{
            case .clear_sky:
                return #imageLiteral(resourceName: "clear_sky")
            case .few_clouds:
                return #imageLiteral(resourceName: "few_clouds")
            case .scatter_clouds:
                return #imageLiteral(resourceName: "scatter_clouds")
            case .broken_clouds:
                return #imageLiteral(resourceName: "clear_sky")
            case .shower_rain:
                return #imageLiteral(resourceName: "shower_rain")
            case .rain:
                return #imageLiteral(resourceName: "rain")
            case .thunderstorm:
                return #imageLiteral(resourceName: "thunderstorm")
            case .snow:
                return #imageLiteral(resourceName: "snow")
            case .mist:
                return #imageLiteral(resourceName: "mist")
            }
        }
    }
    
    
    @objc func getPreview(town:NSString)->Preview?{
        for p in previewArray{
            if (p.town == town) {
                return p
            }
        }
        return nil
    }

    @objc func fetchWeatherPreview(town : NSString, completion: @escaping (NSString?) -> ()) {
        print("start - fetchWeatherPreview")
        self.previewArray.removeAll()
        let objurl = URL(string: String(format:"https://api.openweathermap.org/data/2.5/weather?q=%@&APPID=4bc1f4ca7650ed56f0e2782424f660e3",town))
        let preview = Preview()
        URLSession.shared.dataTask(with: objurl!) {(data, response, error) in
                preview.town = town
                if let data = data {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] {
                            if let main = json["main"] as? [String:Any] {
                                if let kelvinTemp = main["temp"] as? Double {
                                    let celsiusTemp = kelvinTemp - 273.15
                                    preview.temperature = NSString(format: "%.0f°C", celsiusTemp)
                                }
                                if let timezone = json["timezone"] as? Int {
                                    let date = Date()
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "HH:mm"
                                    dateFormatter.timeZone = TimeZone.init(secondsFromGMT: timezone)
                                    preview.hour = NSString(string: dateFormatter.string(from: date))
                                }
                                if let weather = json["weather"] as? [[String:Any]]{
                                    let description = weather[0]["description"] as? NSString
                                    preview.descr = description
                                    preview.icon = weather[0]["icon"] as? NSString
                                }
                                
                            }
                        }
                        
                        completion("Success")
                    } catch {
                        print("Error in fetchWeatherPreview")
                        completion("Error")
                    }
                    self.previewArray.append(preview)
                    //completion()
                }
            
        }.resume()
            
        
    }
    
    func fetchforecast(town : NSString, completion: @escaping ([Weather?]) -> ()){
         let objurl = URL(string: String(format: "https://api.openweathermap.org/data/2.5/forecast?q=%@&APPID=4bc1f4ca7650ed56f0e2782424f660e3", town))
        var forecast = [Weather]()
        URLSession.shared.dataTask(with: objurl!) {(data, response, error) in
            var weather = Weather()
            if let data = data {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] {
                        if let list = json["list"] as? [[String:Any]]{
                            for obj in list {
                                if let main = obj["main"] as? [String:Any] {
                                    weather.temp = self.fromKelvinToCelsius(main["temp"] as? Double)
                                    weather.minTemp = self.fromKelvinToCelsius(main["temp_min"] as? Double)
                                    weather.maxTemp = self.fromKelvinToCelsius(main["temp_max"] as? Double)
                                }
                                if let time = obj["dt"] as? Int {
                                    let date = Date(timeIntervalSince1970: TimeInterval(time))
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "EEEE HH:mm"
                                    weather.dt = dateFormatter.string(from: date)
                                }
                                if let w = obj["weather"] as? [[String:Any]]{
                                    if let d = w[0]["description"] as? String {
                                         weather.description = d
                                    }
                                    weather.icon = w[0]["icon"] as? String
                                   
                                }
                                forecast.append(weather)
                            }
                        }
                    }
                    completion(forecast)
                } catch {
                    print("Error in fetchforecast")
                    completion([Weather]())
                }
                
            }
        }.resume()
    }
    
    private func fromKelvinToCelsius(_ value: Double?) -> String {
        guard let value = value else {return "--°C"}
        let celsiusTemp = value - 273.15
        return String(format: "%.0f°C", celsiusTemp)
    }
    
}

struct Weather{
    var dt : String
    var temp : String
    var maxTemp : String
    var minTemp : String
    var humidity : String
    var description : String
    var icon : String?
    
    init(){
        self.dt = "--"
        self.temp = "--"
        self.maxTemp = "--"
        self.minTemp = "--"
        self.humidity = "--"
        self.description = ""
    }
}

@objc class  Preview: NSObject {
    @objc var town : NSString
    @objc var temperature : NSString
    @objc var hour : NSString
    @objc var imageUrl: NSString?
    @objc var descr: NSString?
    @objc var icon: NSString?
    
    @objc override init() {
        self.town = "--"
        self.temperature = "--"
        self.hour = "--"
    }
    
    @objc init(town: NSString, temperature: NSString, hour: NSString) {
        self.town = town
        self.temperature = temperature
        self.hour = hour
        
    }
}

