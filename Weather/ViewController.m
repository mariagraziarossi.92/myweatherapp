//
//  ViewController.m
//  WeatherApp
//
//  Created by Mariagrazia Rossi on 20/02/2020.
//  Copyright © 2020 mariagrazia. All rights reserved.
//

#import "ViewController.h"
#import "CustomCell.h"
#import "Weather-Swift.h"

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>{

}

@end


@implementation ViewController

NSArray* arrayWithTownArray;
WeatherManager* wManager;
Preview* c;
UIActivityIndicatorView* spinner;


- (void)viewDidLoad {
    [super viewDidLoad];
    wManager = [[WeatherManager alloc] init];
    arrayWithTownArray = @[@"Turin,it",@"Nice,fr",@"London,uk",@"Sydney,au"];
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self startSpinner];
    [self setupTableView:true];
    [self updateData];
}

-(void) updateData{
    for (int i = 0; i < [arrayWithTownArray count]; i++) {
        [wManager fetchWeatherPreviewWithTown:arrayWithTownArray[i] completion:^(NSString* res ){
            if ([res  isEqual: @"Error"]){
                [self showAlertAndExit];
            }
            if (i == [arrayWithTownArray count]-1){
                [self stopSpinner];
                [self setupTableView:false];
            }
        }];
    }
}

-(void)showAlertAndExit{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Data cannot be retrieved."
                                                                   message:@"Maybe your API keys expired"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)setupTableView:(BOOL)hidden{
     dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        [self.tableView setHidden:hidden];
     });
}

-(void)startSpinner{
    spinner.center = self.view.center;
    spinner.tag = 12;
    [self.view addSubview:spinner];
    [spinner startAnimating];
    [self.view setUserInteractionEnabled:false];
}

-(void)stopSpinner{
    dispatch_async(dispatch_get_main_queue(), ^{
        [spinner stopAnimating];
        [spinner removeFromSuperview];
        [self.view setUserInteractionEnabled:true];
    });
}

//MARK: tableView configuration
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CityCell"];
    Preview *c = [wManager getPreviewWithTown:arrayWithTownArray[indexPath.row]];
    [cell configure:c];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrayWithTownArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DetailViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    vc.cityPreview = [wManager getPreviewWithTown:arrayWithTownArray[indexPath.row]];
    [self.navigationController pushViewController:vc animated:false];
}



@end

